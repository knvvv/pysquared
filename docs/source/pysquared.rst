pysquared package
=================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pysquared.dataitems
   pysquared.planner
   pysquared.storage
   pysquared.threadmanager
   pysquared.transforms
   pysquared.worker
   pysquared.workflow

Submodules
----------

pysquared.sidebar\_ui module
----------------------------

.. automodule:: pysquared.sidebar_ui
   :members:
   :undoc-members:
   :show-inheritance:

pysquared.utils module
----------------------

.. automodule:: pysquared.utils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pysquared
   :members:
   :undoc-members:
   :show-inheritance:
