# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import sys, os
sys.path.insert(0, os.path.abspath('../'))

project = 'PySquared'
copyright = '2024, Nikolai Krivoshchapov'
author = 'Nikolai Krivoshchapov'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.viewcode",
    "sphinx.ext.intersphinx",
    "sphinx.ext.autosummary",
    "sphinx.ext.napoleon",
]

autosummary_generate = True
source_suffix = ".rst"

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

pygments_style = "sphinx"
todo_include_todos = False
language = 'en'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'furo'

htmlhelp_basename = "pysquareddoc"

master_doc = "index"

intersphinx_mapping = {
    'python': ('http://docs.python.org/', None),
    'pandas': ('http://pandas.pydata.org/pandas-docs/dev', None)
}
autoclass_content = 'both'

autodoc_default_options = {
    "members": True,
    "undoc-members": True,
    "private-members": True,
    'special-members': True
}