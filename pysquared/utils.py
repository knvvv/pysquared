import logging
import inspect


def add_parameters(*params):
    def decorator(func):
        original_signature = inspect.signature(func)

        # Create a list of parameters to be added
        new_params = [inspect.Parameter(name, inspect.Parameter.POSITIONAL_OR_KEYWORD) for name in params]

        # Extend the original parameters with the new ones
        new_signature = original_signature.replace(parameters=new_params + list(original_signature.parameters.values()))

        # Update the function with the new signature
        func.__signature__ = new_signature

        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)

        return wrapper

    return decorator


def is_float(inp: str) -> bool:
    """Basic utility to check whether ``inp`` string can be represented as a ``float``. Note that the result will also be positive for ints!

    Args:
        inp (str): the string of interest

    Returns:
        bool: ``True`` if inp represent ``float`` type. ``False`` otherwise.
    """
    try:
        float(inp)
        return True
    except:
        return False


def is_int(inp: str) -> bool:
    """Basic utility to check whether ``inp`` string can be represented as a ``int``.

    Args:
        inp (str): the string of interest

    Returns:
        bool: ``True`` if inp represent ``int`` type. ``False`` otherwise.
    """
    try:
        int(inp)
        return True
    except:
        return False


def str_to_valuetype(value: str) -> str | int | float:
    """This function decides between three base types stored as keys in DataStorage: ``int``, ``float``, ``str``. For example, it converts "2.5" to float 2.5, "5" to int 5, and arbitrary stringz are kept the same.

    Args:
        value (str): input string

    Returns:
        str | int | float: the converted object (``int``, ``float``, or ``str``)
    """
    if is_int(value):
        return int(value)
    elif is_float(value):
        return float(value)
    else:
        return value


def create_logger(classname: str, stream=None, filename="mylog.log") -> logging.Logger:
    logger = logging.getLogger(classname)
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter("%(name)s:%(levelname)s  %(message)s")

    if filename is not None:
        file_handler = logging.FileHandler(filename)
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(formatter)

    stream_handler = logging.StreamHandler(stream)
    stream_handler.setLevel(logging.DEBUG)
    stream_handler.setFormatter(formatter)

    logger.addHandler(stream_handler)
    if filename is not None:
        logger.addHandler(file_handler)
        
    return logger


class DotDict:
    """Implementation of dot access to dict keys. E.g. you have a dict ``foo = {'a': x}`` and want to save time typing ``foo["a"]``. You can just create a ``bar = DotDict(foo)`` and use ``bar.a``
    DotDict is used to implement ``LoggerShortcut``.
    """
    def __init__(self, dictionary: dict) -> None:
        """Take a dict that we are going to access later from DotDict. Note that there is not simple way to add new keys.

        Args:
            dictionary (dict): The base dict
        """
        self.__dict__.update(dictionary)


# TODO Use namedtuple?
class LoggerShortcut(DotDict):
    """Stores four lambdas that are responsible for various logging levels. Can call them as ``log.warning(message)`` etc. (All levels: 'debug', 'info', 'warning', 'error')
    """
    def __init__(self, logger: logging.Logger | None) -> None:
        """This class serves for convenient logging or suppressed logging (if got None instead of the ``logger``).
        This is used all over the codebase to skip the checks if the logging is enabled/disabled before calling the logger.

        Args:
            logger (logging.Logger | None): Logger object (if logging is enabled), or None (if logging is disabled, then all messages are suppressed)
        """
        if logger is not None:
            init_dict = {
                'debug': lambda message: logger.debug(message),
                'info': lambda message: logger.info(message),
                'warning': lambda message: logger.warning(message),
                'error': lambda message: logger.error(message),
            }
        else:
            init_dict = {
                'debug': lambda message: None,
                'info': lambda message: None,
                'warning': lambda message: None,
                'error': lambda message: None,
            }
        super().__init__(init_dict)
