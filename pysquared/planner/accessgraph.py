from typing import List, Iterator

import networkx as nx
import pandas as pd

from ..utils import LoggerShortcut

class AccessGraph:
    def __init__(self, inp_g, node_set, logger=None):
        self.log = LoggerShortcut(logger)

        self.g = nx.DiGraph()
        self.g.add_nodes_from(node_set)
        for nodeA, nodeB in inp_g.edges:
            if nodeA in node_set and nodeB in node_set:
                self.g.add_edge(nodeA, nodeB)
        self.log.debug(f"AccessGraph nodes = {repr(list(self.g.nodes))}")
        self.log.debug(f"AccessGraph edges = {repr(list(self.g.edges))}")
        
        for node in self.g.nodes:
            self.g.nodes[node]['is_item'] = inp_g.nodes[node]['is_item']
            if self.g.nodes[node]['is_item']:
                if 'item' in inp_g.nodes[node] and inp_g.nodes[node]['item'].non_empty:
                    self.g.nodes[node]['known'] = True
                    self.g.nodes[node]['active'] = True
                    self.log.debug(f"Node '{node}' is known")
                else:
                    self.g.nodes[node]['known'] = False
                    self.g.nodes[node]['active'] = False
                    self.log.debug(f"Node '{node}' is not known")
            else:
                self.g.nodes[node]['active'] = False
        
        self.log.debug("Before propagation:\n" + self.get_state())
        converged = False
        while not converged:
            converged = True
            for node in self.g.nodes:
                if not self.g.nodes[node]['is_item']:
                    all_inputs_active = True
                    for nb in self.g.predecessors(node):
                        all_inputs_active = all_inputs_active and self.g.nodes[nb]['active']
                    
                    if not self.g.nodes[node]['active'] and all_inputs_active:
                        self.g.nodes[node]['active'] = True
                        converged = False
                    elif self.g.nodes[node]['active'] and not all_inputs_active:
                        self.g.nodes[node]['active'] = False
                        converged = False
                
            for node in self.g.nodes:
                if self.g.nodes[node]['is_item']:
                    some_source_is_active = False
                    for nb in self.g.predecessors(node):
                        some_source_is_active = some_source_is_active or self.g.nodes[nb]['active']

                    if not self.g.nodes[node]['active'] and some_source_is_active:
                        self.g.nodes[node]['active'] = True
                        converged = False
        self.log.debug("After propagation:\n" + self.get_state())
    
    def paths_iterate(self,
            target: str,
            force_avoid_regeneration: bool=True
        ) -> Iterator[List[str]]:
        """Iterate over all possible paths to reach the target ``target``.

        Args:
            target (str): Name (key of NetworkX node) of the desired node of transform graph (by agreement, use ``str``)
            force_avoid_regeneration (bool, optional): Avoid yielding sequences that involve transforms creating already known DataItems. Exceptions are the transforms that generate the ``target`` itself (``target`` is allowed to be known and then overwritten during transform sequence). Defaults to True.

        Yields:
            Iterator[List[str]]: Generator of possible sequences of transforms in the order of their execution (from first to last).
        """
        # Each prospect is a sequence of transforms that lead to the desired DataItem in the REVERSED order of execution (from last to first)
        prospects: List[List[str]] = [
            [node]
            for node in self.g.predecessors(target)
            if self.g.nodes[node]['active']
        ]

        while len(prospects) > 0:
            # Process one prospect per 'while'-loop iteration
            cur_prospect: List[str] = prospects.pop()
            # ``cur_prospect`` is not guaranteed to be totally sensible. It is decided on this iteration
            # ``cur_prospect`` will either be extended+stored, yielded, or discarded

            loose_ends: List[str] = []
            all_ends: List[str] = []

            incorrect_prospect = False # If True, then discard ``cur_prospect``

            # Iterate over ALL dataitems & transforms in the graph,
            # But interested only in source dataitems of transforms from ``cur_prospect``
            for node in self.g.nodes:
                # Indices of transforms in ``cur_prospect`` that use ``node`` as source dataitem
                employing_transforms_idxs: List[int] = [
                    cur_prospect.index(employing_transform)
                    for employing_transform in self.g.successors(node)
                    if employing_transform in cur_prospect
                ]

                # Decide whether ``node`` is indeed a source dataitem of at least one transform of ``cur_prospect``
                if len(employing_transforms_idxs) == 0:
                    continue
                
                # Indices of transform(s) in ``cur_prospect`` that generate ``node`` (i.e., have ``node`` as target)
                forming_idxs: List[int] = [
                    cur_prospect.index(forming_transform)
                    for forming_transform in self.g.predecessors(node)
                    if forming_transform in cur_prospect
                ]
                n_formed = len(forming_idxs) # expect it to be 0 or 1
                
                if n_formed > 1:
                    # Definitely skip when ``node`` is formed from several transforms in the prospect
                    incorrect_prospect = True
                elif n_formed == 1:
                    # Also skip if any of transforms that USE ``node`` are executed before it is generated
                    forming_idx = forming_idxs[0]
                    incorrect_prospect: bool = any(
                        employing_transform_idx > forming_idx
                        for employing_transform_idx in employing_transforms_idxs
                    )
                else:
                    # This is covered only when ``node`` is NOT yet formed by any transform in ``cur_prospect``
                    all_ends.append(node)
                    if not self.g.nodes[node]['known']:
                        loose_ends.append(node)
                
                if incorrect_prospect:
                    break
            
            if incorrect_prospect:
                cur_prospect.reverse()
                self.log.warning(f"Incorrect prospect was encountered and ignored: {repr(cur_prospect)}")
                continue 

            if len(loose_ends) == 0: # Yield a final and checked sequence
                ret_list = list(reversed(cur_prospect))
                yield ret_list
            
            # Extend ``cur_prospect`` if there are loose ends
            for item_node in all_ends:
                for next_transform in self.g.predecessors(item_node):
                    # Cheap checks that ``next_transform`` is not acceptable
                    if not self.g.nodes[next_transform]['active'] or next_transform in cur_prospect:
                        continue
                    
                    if force_avoid_regeneration and self.is_transform_regenerating(next_transform):
                        continue

                    new_prospect = [*cur_prospect, next_transform]
                    if new_prospect not in prospects:
                        prospects.append(new_prospect)
        
    def is_transform_regenerating(self, next_transform: List[str]) -> bool:
        return all(
            self.g.nodes[dataitem_name]['known']
            for dataitem_name in self.g.successors(next_transform)
        )
    
    def get_state(self):
        data = []
        for node in self.g.nodes:
            data.append({**{'node': node}, **{key: value for key, value in self.g.nodes[node].items()}})
        df = pd.DataFrame(data)
        return repr(df[df['is_item']])
    
    def get_all_ends(self, seq: List[str]) -> List[str]:
        """Get all DataItems that are used as inputs of the given transform sequence ``seq``

        Args:
            seq (List[str]): Transform sequence of interest (usually, returned from ``self.paths_iterate``). Ordering from first to last.

        Returns:
            List[str]: List of DataItems (in arbitrary order) that will be used as sources in the given transform sequence ``seq``
        """
        # Convert ``seq`` to the ordering "from last to first". This creates a reversed copy
        seq: List[str] = list(reversed(seq))

        # Container for "ends" of the transform sequence
        # DataItems that are sources of some transforms and not generated by any other transform in ``seq``
        all_ends: List[str] = []

        for node in self.g.nodes:
            # Indices of transforms in ``seq`` that use ``node`` as source dataitem
            employing_transforms_idxs: List[int] = [
                seq.index(employing_transform)
                for employing_transform in self.g.successors(node)
                if employing_transform in seq
            ]

            # Decide whether ``node`` is indeed a source dataitem of at least one transform of ``seq``
            if len(employing_transforms_idxs) == 0:
                continue
            
            # Number of transform(s) in ``seq`` (either 1 or 0) that generate ``node`` (i.e., have ``node`` as target)
            n_formed = sum(
                1
                for forming_transform in self.g.predecessors(node)
                if forming_transform in seq
            )
            assert n_formed == 0 or n_formed == 1, \
                f"Number of transforms forming '{node}' must be either 1 or 0. Got {n_formed}"

            # Skipped some checks here, but they must have been done in ``self.paths_iterate`` anyways
            
            if n_formed == 0:
                all_ends.append(node)
        return all_ends
