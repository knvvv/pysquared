from typing import List, Never

import networkx as nx

from .accessgraph import AccessGraph
from .transform_concrete import TransformConcrete
from .transform_executor import TransformExecutor, TransformState, TransformResult
from ..transforms import Transform
from ..utils import LoggerShortcut
from ..workflow import InstructionFactories as instr
from ..workflow.stack import HandlerStatus

class Transformator:
    def __init__(self,
            storage,
            transformations=[],
            logger=None
        ):

        self.storage = storage
        self.logger = logger
        self.log = LoggerShortcut(logger)

        # Hack when lists/tuples are included
        new_transformations = []
        known_transforms = []
        for x in transformations:
            if isinstance(x, tuple) or isinstance(x, list):
                for tr in x:
                    assert tr.NAME not in known_transforms, \
                        f"Attempting to register transform '{tr.NAME}' twice"
                    new_transformations.append(tr)
                    known_transforms.append(tr.NAME)
            else:
                assert x.NAME not in known_transforms, \
                    f"Attempting to register transform '{x.NAME}' twice"
                new_transformations.append(x)
                known_transforms.append(x.NAME)
        transformations = new_transformations

        for t in transformations:
            assert t.NAME != Transform.DEFAULTS['NAME'], \
                f"Attempting to construct Transformator from untitled transformation '{t.__name__}'"
        self.transformations = {t.NAME: t for t in transformations}
        self.graph = nx.DiGraph()

        get_node_type = lambda node_name: ('item' if self.graph.nodes[node_name]['is_item'] else 'transform')
        def assert_available_itemnode(node_name: str) -> None | Never:
            node_exists = self.graph.has_node(node_name)
            node_is_item = node_exists and self.graph.nodes[node_name]['is_item']
            assert not node_exists or node_is_item, (
                f"The requested item name '{node_name}' is already occupied by a transform"
            )

        for tr_name, tr_obj in self.transformations.items():
            assert not self.graph.has_node(tr_name), \
                f"Name of the requested transform '{tr_name}' is already registered in the pipeline as {get_node_type(tr_name)}"
            self.graph.add_node(tr_name, is_item=False, transform=tr_obj)

            for source in tr_obj.SOURCE_ITEMS:
                assert_available_itemnode(source)
                self.graph.add_edge(source, tr_name, transform=tr_obj)
                self.graph.nodes[source]['is_item'] = True
            for target in tr_obj.TARGET_ITEMS:
                assert_available_itemnode(target)
                self.graph.add_edge(tr_name, target, transform=tr_obj)
                self.graph.nodes[target]['is_item'] = True
            for note_item in tr_obj.NOTE_ITEMS:
                assert_available_itemnode(note_item)
                self.graph.add_node(note_item, is_item=True)

        for source, target in self.graph.edges:
            assert self.graph.nodes[source]['is_item'] ^ self.graph.nodes[target]['is_item'], (
                "Connections are only allowed between pairs item-transform "
                f"({source} -> {target}, {source} is {get_node_type(source)}, {target} is {get_node_type(target)})."
            )

        for item_node in self.graph.nodes:
            if not self.graph.nodes[item_node]['is_item']:
                continue
            
            assert item_node in self.storage, f"Unregistered item '{item_node}'"

            self.graph.nodes[item_node]['item'] = getattr(storage, item_node)
        
        for item_name, _ in self.storage:
            if not self.graph.has_node(item_name):
                self.log.error(f"Item '{item_name}' is not involved in any transforms")
            
    def _find_path_to(self, target_name: str, prompt_transformator_path: bool=False) -> list:
        assert self.graph.has_node(target_name), \
            f"Transform graph does not contain the target node '{target_name}'. Known nodes={[node for node in self.graph.nodes]}"

        # Isolate connected component containing the target
        active_graph = nx.Graph()
        active_graph.add_edges_from(self.graph.edges)
        active_graph.add_nodes_from(self.graph.nodes)
        for comp in nx.connected_components(active_graph):
            if target_name in comp:
                main_component_set = comp
                break
        
        ag = AccessGraph(self.graph, main_component_set, logger=self.logger)

        found_seqs = []
        starting_items = []
        for seq in ag.paths_iterate(target_name):
            found_seqs.append(seq)
            starting_items.append(ag.get_all_ends(seq))
            break
        
        # I don't see the need for external path selectors at this point
        assert len(found_seqs) != 0, f"No route to target '{target_name}' was found"
        assert not len(found_seqs) > 1, f"Several paths to the target '{target_name}' were found. {repr(found_seqs)}"
        found_seq = found_seqs[0]

        starting_items = starting_items[0]
        start_items_message = "Initial DataItems: " + ", ".join(starting_items)
        target_item_message = f"Target DataItem: {target_name}"
        seq_message = "Sequence of transforms: " + " => ".join(found_seq)
        self.log.info("""\n
{sep}
{start_items_message}
{target_item_message}
{seq_message}
{sep}  
""".format(
                start_items_message=start_items_message,
                target_item_message=target_item_message,
                seq_message=seq_message,
                sep=''.join(['-'] * len(seq_message))
            )
        )
        if prompt_transformator_path:
            import click
            click.confirm('Do you confirm this transformator route?', default=True, abort=True)

        redundant_steps: List[str] = []
        # [:-1] allows the final target itself to be regenerated (since the user specifically required it)
        for transform_name in found_seq[:-1]:
            all_are_known = True
            for target_item_name in ag.g.successors(transform_name):
                all_are_known = all_are_known and ag.g.nodes[target_item_name]['known']
            if all_are_known:
                redundant_steps.append(transform_name)
        assert len(redundant_steps) == 0, \
            f"Internal error in transformator planning: found {len(redundant_steps)} redundant steps {redundant_steps}"

        return found_seq, starting_items
    
    def contains_dataitem(self, item_name: str) -> bool:
        return self.graph.has_node(item_name) and self.graph.nodes[item_name]['is_item']

    def plan_transformation(self,
            stack_frame,
            transformator_name: str,
            target: str,
            sources: list=None,
            forward: dict={}
        ) -> None:

        if sources is not None:
            raise NotImplementedError
        find_path_kwargs = {}
        if 'prompt_transformator_path' in forward:
            find_path_kwargs['prompt_transformator_path'] = forward['prompt_transformator_path']
        transform_path, start_items = self._find_path_to(target, **find_path_kwargs)

        # TODO Do some checks with startitems

        instructions = []
        for transform_name in transform_path:
            instruction = instr.prepare_transform(
                transformator=transformator_name,
                transform=transform_name,
                path=transform_path,
                forward=forward
            )
            instruction.info.activates = []
            instruction.info.activated_by = []
            instructions.append(instruction)
        for prev_tr, next_tr in zip(instructions, instructions[1:]):
            prev_tr.info.activates.append(next_tr.info.name)
            next_tr.info.activated_by.append(prev_tr.info.name)
        stack_frame.include_instructions(instructions)

    def prepare_transform(self,
            stack_frame,
            transformator_name: str,
            transform_name: str,
            transform_path: list,
            forward: dict={}
        ) -> None:

        transform_inst = TransformConcrete(
            transformator=self,
            transform_name=transform_name,
            logger=self.logger
        )
        transform_inst.prepare_instance(forward=forward)
        
        instructions = []
        for key_combination in transform_inst.get_key_combinations():
            # By default, entry point is the 'exec'-method for all transform
            instruction = instr.execute_transform(
                transformator=transformator_name,
                transform=transform_name,
                unmerged_keys=key_combination,
                forward=forward
            )
            instruction.info.activates = []
            instruction.info.activated_by = []
            instructions.append(instruction)
        
        stack_frame.include_instructions(instructions)
        stack_frame.info.transform_instance = transform_inst
    
    def execute_transform(self,
            stack_frame,
            instruction,
            transformator_name: str,
            transform_name: str,
            unmerged_keys: dict,
            forward: dict={}
        ) -> HandlerStatus:

        if 'keys_control' in forward and not forward['keys_control'](unmerged_keys):
            return HandlerStatus.DONE # Skip transform if user requested so
        
        if 'transform_executor' not in instruction.info:
            instruction.info.transform_executor = TransformExecutor(
                transform_instance=stack_frame.info.transform_instance,
                transformator_name=transformator_name,
                transform_name=transform_name,
                logger=self.logger
            )
        transform_executor = instruction.info.transform_executor

        result: TransformResult = transform_executor.execute(
            unmerged_keys=unmerged_keys,
            forward=forward,
        )

        if result == TransformResult.FINISHED or result == TransformResult.FAILED:
            return HandlerStatus.DONE
        elif result == TransformResult.REPEAT:
            return HandlerStatus.REPEAT
        elif result == TransformResult.LATER:
            return HandlerStatus.LATER
        else:
            raise ValueError(f"Cannot process transform result '{result}'")
