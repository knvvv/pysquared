import copy
import inspect
import itertools

from ..utils import LoggerShortcut
from ..transforms import Transform

import pandas as pd


class TransformConcrete:
    def __init__(self,
            transformator,
            transform_name: str,
            logger=None
        ) -> None:
        
        self.transformator = transformator
        self.transform_name = transform_name

        self.logger = logger
        self.log = LoggerShortcut(logger)
    
    def prepare_instance(self, forward: dict={}) -> None:
            
        self.log.info(f"Preparing for '{self.transform_name}' transform")
        self.cur_transform: Transform = self.transformator.transformations[self.transform_name]

        self.args_list = {
            method_name: inspect.getfullargspec(method)[0]
            for method_name, method in self.cur_transform.methods.items()
        }
        assert 'exec' in self.args_list, \
            f"Entry point ('exec'-method) is not implemented for transform '{self.transform_name}'"
        
        aware_keys = self.cur_transform.AWARE_KEYS
        merged_keys = self.cur_transform.MERGED_KEYS
        assert len(set(aware_keys).intersection(merged_keys)) == 0, \
            f"Aware and Merge sets overlap. Aware={repr(aware_keys)}, Merged={repr(merged_keys)}"

        # Construct lists of keys
        self.keys_lists = {}
        self.source_keys = []
        for source_name in self.cur_transform.SOURCE_ITEMS:
            self.keys_lists[source_name] = self.transformator.graph.nodes[source_name]['item'].public_keys
            for key in self.keys_lists[source_name]:
                if key not in self.source_keys:
                    self.source_keys.append(key)
        for cur_target_name in self.cur_transform.TARGET_ITEMS:
            self.keys_lists[cur_target_name] = self.transformator.graph.nodes[cur_target_name]['item'].public_keys
        for note_name in self.cur_transform.NOTE_ITEMS:
            self.keys_lists[note_name] = self.transformator.graph.nodes[note_name]['item'].public_keys
        self.log.debug("keys_lists = " + repr(self.keys_lists))

        # Check that requested keys are present
        aware_okay = True
        for key in aware_keys:
            if key not in self.source_keys:
                aware_okay = False
                self.log.error(f"Requested aware key '{key}' is not involved in transform '{self.transform_name}'")
        merged_okay = True
        for key in merged_keys:
            if key not in self.source_keys:
                merged_okay = False
                self.log.error(f"Requested merged key '{key}' is not involved in transform '{self.transform_name}'")
        assert aware_okay and merged_okay, f"Failed key check on the stage '{self.transform_name}'"
        
        # Check for overlapping naming of keys, items, forwarded args, etc.
        itemnames_set = set()
        keys_set = set()
        forward_stuff = list(forward.keys())
        for l in self.keys_lists.values():
            keys_set.update(set(l))
        for l in [self.cur_transform.SOURCE_ITEMS, self.cur_transform.TARGET_ITEMS, self.cur_transform.NOTE_ITEMS]:
            itemnames_set.update(set(l))

        def check_overlap(**attrs):
            assert len(attrs) == 2
            names = list(attrs.keys())
            sets = list(attrs.values())
            intersection = sets[0].intersection(sets[1])
            assert len(intersection) == 0, f"{names[0]} and {names[1]} share element(s): {repr(intersection)}"
        check_overlap(item_names=itemnames_set, key_names=keys_set)
        check_overlap(item_names=itemnames_set, forwarded_keys=forward_stuff)
        check_overlap(key_names=keys_set, forwarded_keys=forward_stuff)

        # Check prerequisites for unaware, aware and merged keys
        for key in self.source_keys:
            each_source = True
            each_target = True
            one_source = False
            one_target = False # "one_target and not one_source" is a criteria for new key
            for source_name in self.cur_transform.SOURCE_ITEMS:
                if key in self.keys_lists[source_name]:
                    one_source = True
                else:
                    each_source = False
            for cur_target_name in self.cur_transform.TARGET_ITEMS:
                if key in self.keys_lists[cur_target_name]:
                    one_target = True
                else:
                    each_target = False
            
            if key in aware_keys: # aware
                assert each_target and one_source, f"Necessary condition for aware key is not satisfied for '{key}'"
                self.log.debug(f"Key '{key}' is in aware mode")
            elif key in merged_keys: # merged
                assert one_source, f"Necessary condition for merged key is not satisfied for '{key}'"
                self.log.debug(f"Key '{key}' is in merged mode")
            else: # unaware
                assert each_source and each_target, f"Necessary condition for unaware key is not satisfied for '{key}'"
                self.log.debug(f"Key '{key}' is in unaware mode")

        self.nonmerged_keys = copy.copy(self.source_keys)
        for key in merged_keys:
            del self.nonmerged_keys[self.nonmerged_keys.index(key)]
        if self.transform_name == 'get_nbo_cube':
            print('HERE')

    def _key_combinations(self, items_names: list[str], nonmerged_keys: list[str]) -> list[dict[str, any]]:
        split_items = {}
        for itemname in items_names: # get_item
            item = self.transformator.storage.get_item(itemname)
            kv_maps = item.get_keyvalue_entries()
            split_items[itemname] = [
                {
                    key: value
                    for key, value in kv_map.items()
                    if key in nonmerged_keys
                }
                for kv_map in kv_maps
            ] # {item_name => [element_index => {key => value}]}

        # {key => [merged_index => value]}
        key_columns: dict[str, list] = {key: [] for key in nonmerged_keys}

        # {item_name => [merged_index => [element_index list]]}
        item_columns: dict[str, list[set[int]]] = {item_name: [] for item_name in items_names}

        for itemname, item_elements_list in split_items.items():
            for elem_index, kv_map in enumerate(item_elements_list):
                for key in nonmerged_keys:
                    if key in kv_map:
                        key_columns[key].append(kv_map[key])
                    else:
                        key_columns[key].append(None)
                item_columns[itemname].append({elem_index})
                for cur_item_name in items_names:
                    if cur_item_name != itemname:
                        item_columns[cur_item_name].append(set())

        def keys_from_index(index: int) -> dict[str, any]:
            return {
                key: value_list[index]
                for key, value_list in key_columns.items()
            }
        
        def items_from_index(index: int) -> dict[str, set[int]]:
            return {
                key: value_list[index]
                for key, value_list in item_columns.items()
            }

        def remove_row_index(index: int) -> None:
            for cur_list in itertools.chain(key_columns.values(), item_columns.values()):
                del cur_list[index]
        
        def get_united_size() -> int:
            num_elements_lists = set(
                len(cur_list)
                for cur_list in itertools.chain(key_columns.values(), item_columns.values())
            )
            assert len(num_elements_lists) == 1
            return num_elements_lists.pop()
        
        def entry_extends(check_entry: dict[str, any], ref_entry: dict[str, any]) -> bool:
            return all(
                ref_entry[key] == value
                for key, value in check_entry.items()
                if value is not None
            )
        
        def merge_entries(source_index: int, target_index: int) -> None:
            source_items: dict[str, set[int]] = items_from_index(source_index)
            for itemname, source_indices in source_items.items():
                item_columns[itemname][target_index].update(source_indices)

        def build_united_df() -> pd.DataFrame:
            key_to_column = lambda key: 'key_' + key
            itemname_to_column = lambda key: 'item_' + key
            united_keys = {
                **{key_to_column(key): value_list for key, value_list in key_columns.items()},
                **{itemname_to_column(itemname): [tuple(indices) for indices in value_list] for itemname, value_list in item_columns.items()},
            }

            united_df = pd.DataFrame(united_keys)
            # for itemname in items_names:
            #     united_df[itemname_to_column(itemname)] = pd.to_numeric(united_df[itemname_to_column(itemname)], errors='coerce').astype('Int64')
            return united_df

        def keys_to_str(keys: dict[str, any]) -> str:
            return ', '.join(
                f"{key}={value}"
                for key, value in keys.items()
                if value is not None
            )

        def repr_row(row_index: int) -> str:
            keys = keys_from_index(row_index)
            defined_by_message = '\n'.join(
                f"    {item_name}: {', '.join(
                    keys_to_str(split_items[item_name][elem_index])
                    for elem_index in elem_indices
                )}"
                for item_name, elem_indices in items_from_index(row_index).items()
                if len(elem_indices) > 0
            )
            return f"""\
{keys_to_str(keys)} defined by
{defined_by_message}"""

        def repr_several_rows(row_indices: list[int]) -> str:
            message_parts = [
                f'{i}) {repr_row(row_index)}'
                for i, row_index in enumerate(row_indices, start=1)
            ]
            rows_message = '\n'.join(message_parts)
            return rows_message

        initial_united_df: pd.DataFrame = build_united_df()
        self.log.info(f"Preliminary matrix of key combinations:\n{initial_united_df}")

        check_row_index = 0
        while check_row_index < get_united_size():
            extending_element = False
            check_keys = keys_from_index(check_row_index)

            for ref_row_index in range(get_united_size()):
                if ref_row_index == check_row_index:
                    continue
                ref_keys = keys_from_index(ref_row_index)
                if entry_extends(check_keys, ref_keys):
                    # self.log.info(f"Merging these elements:\n{repr_several_rows([check_row_index, ref_row_index])}")
                    merge_entries(check_row_index, ref_row_index)
                    extending_element = True

            if extending_element:
                remove_row_index(check_row_index)
            else:
                check_row_index += 1


        if all(
            (None not in key_list) and all(len(indices) > 0 for indices in indices_list)
            for key_list, indices_list in zip(key_columns.values(), item_columns.values())
        ):
            self.log.info(f"Final matrix of key combinations:\n{build_united_df()}")
            return [
                {
                    key: value_list[i]
                    for key, value_list in key_columns.items()
                }
                for i in range(get_united_size())
            ]

        missing_keys: dict[tuple[str], int] = {}
        for check_row_index in range(get_united_size()):
            keys = keys_from_index(check_row_index)
            if None not in keys.values():
                continue
            cur_missing_keys: tuple[str] = tuple(sorted([
                key
                for key, value in keys.items()
                if value is None
            ]))
            if cur_missing_keys not in missing_keys:
                missing_keys[cur_missing_keys] = [check_row_index]
            else:
                missing_keys[cur_missing_keys].append(check_row_index)

        missing_items = {}
        for check_row_index in range(get_united_size()):
            items: dict[str, set[int]] = items_from_index(check_row_index)
            if all(len(indices) > 0 for indices in items.values()):
                continue
            
            cur_missing_items: tuple[str] = tuple(sorted([
                itemname
                for itemname, elem_indices in items.items()
                if len(elem_indices) == 0
            ]))
            if cur_missing_items not in missing_items:
                missing_items[cur_missing_items] = [check_row_index]
            else:
                missing_items[cur_missing_items].append(check_row_index)

        error_messages = ['Inconsitencies in nonmerged key combinations are found:']
        for cur_missing_items, failed_row_indices in missing_items.items():           
            error_messages.append(f"""\
No elements of {cur_missing_items} match the requirements:
{repr_several_rows(failed_row_indices)}
""")
        for cur_missing_keys, failed_row_indices in missing_keys.items():
            error_messages.append(f"""\
The following requirements do not specify the key {cur_missing_keys}:
{repr_several_rows(failed_row_indices)}
""")

        final_united_df: pd.DataFrame = build_united_df()
        error_messages.append(f"""\
Complete matrix of key combinations:
{final_united_df}
""")
        self.log.error('\n'.join(error_messages))
        initial_united_df.to_csv('initial_united.csv', sep=';', index=False)
        final_united_df.to_csv('final_united.csv', sep=';', index=False)
        raise Exception("Inconsitencies in nonmerged key combinations are found. See log for details.")

    def get_key_combinations(self):
        return self._key_combinations(
            items_names=self.cur_transform.SOURCE_ITEMS,
            nonmerged_keys=self.nonmerged_keys
        )
