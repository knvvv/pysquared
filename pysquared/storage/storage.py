import os
import copy
import string
import fnmatch
import time
import itertools
from logging import Logger
from pathlib import Path
from typing import List, Dict, Literal, Tuple, Iterable, Any

import numpy as np
import pandas as pd
from ..dataitems import REGISTERED_DATAITEM_TYPES, ColumnType, PathItem
from ..utils import LoggerShortcut


# TODO: Think about splitting DataStorage into DataStorage + Some kind of table storage? There are, obviously, two levels of abstraction implemented in a single class (+ we need further abstraction due to remote data storage)
# TODO: Maybe even a class for separate tables?
# TODO: Storage relocation, reformatting, etc.
class DataStorage:
    def __init__(self,
            item_list: Dict[str, Dict[str, Any]] | None = None,
            logger: Logger | None = None,
            wd: str=None,
            instantiate_prototypes: bool=True,
            allow_overwrite: bool=False,
            allow_lazy_checkin: bool=False,
        ) -> None:
        """The storage is constructed from a dict of all DataItems it's going to store

        Args:
            item_list (Dict[str, Dict[str, Any]] | None, optional): Specification of all DataItems. Defaults to None.
            logger (Logger | None, optional): Either logger or None (if logging is disabled). Defaults to None.
            wd (str, optional): Working directory for the project (all PathItems are stored there). Defaults to None.
            instantiate_prototypes (bool, optional): For now, it's always True. In the future, when scopes are implemented, ``item_list`` will indeed contain just prototypes that are instantiated as DataItems in separated scopes when required. Defaults to True.
            allow_overwrite (bool, optional): Whether we allow to index elements by overwriting values registered earlier. Defaults to False.
        """

        self.data = {}
        
        self.logger = logger
        self.log = LoggerShortcut(logger)
        
        self.additional_kwargs = {}
        if logger is not None:
            self.additional_kwargs['logger'] = logger
        self.additional_kwargs['allow_overwrite'] = allow_overwrite
        self.additional_kwargs['allow_lazy_checkin'] = allow_lazy_checkin
        
        if wd is None:
            self.wd = os.path.abspath('.')
        else:
            self.wd = os.path.abspath(wd)
        self.log.info(f"Using '{self.wd}' as the base directory for the project")

        self._initialize_directories_item()
        self._initialize_modtimes_item()

        self.items_prototypes = copy.copy(item_list)
        self.instantiate_prototypes = instantiate_prototypes

        if item_list is not None and self.instantiate_prototypes:
            for item_name, item_attrs in item_list.items():
                # Rename 'item_type' => 'type' to make client code more compact
                if 'type' in item_attrs:
                    item_attrs['item_type'] = item_attrs['type']
                    del item_attrs['type']

                self._new_item(name=item_name, **item_attrs)
    
    def _new_item(self, name: str, item_type: str, **kwargs) -> None:
        """Initialize a new DataItem for storage.

        Args:
            name (str): Name of the new DataItem
            item_type (str): Type of the DataItem as recorded in ``REGISTERED_DATAITEM_TYPES``
            **kwargs: everything that needs to be passed into constructor of the requeted type of DataItem
        """
        assert name not in self.data, f"Item '{name}' is already registered"

        assert item_type in REGISTERED_DATAITEM_TYPES, \
            f"DataItem type '{item_type}' is not registered " \
            f"(registered types are {repr(tuple(REGISTERED_DATAITEM_TYPES.keys()))})"
        
        pass_kwargs = {
            **kwargs,
            **{
                key: value
                for key, value in self.additional_kwargs.items()
                if key not in kwargs
            }
        }
        data_item = REGISTERED_DATAITEM_TYPES[item_type](name=name, storage=self, **pass_kwargs)

        # TODO: Explain what each case of 'modtime_control' means
        assert hasattr(data_item, 'modtime_control')
        assert isinstance(data_item.modtime_control, dict) and \
                    len(data_item.modtime_control) == 1 and \
                    'column' in data_item.modtime_control or \
                data_item.modtime_control == 'manual'

        self.data[name] = {
            'name': name,
            'type': item_type,
            'item': data_item,
            'table': self._create_table(data_item._key_names) if not data_item.lazy_mode else None,
            'lazily_available': False,
            'modtime_control': data_item.modtime_control, # Maybe it's better to have a method in data_item for this?
            'restrictions': {},
        }
        self.log.info(f"DataItem '{name}' of type '{item_type}' is registered successfully")

        # At this point, ``data_item`` is fully included in the storage
        # If this is a PathItem, we need to create DirectoryItems for hierarchy of containing directories
        data_item.checkin()

        return data_item

    def _initialize_directories_item(self) -> None:
        """Create a dedicated ``DirectoryItem`` (called ``_internal('directories')``) that maps a ``PathItem`` to its base ``DirectoryItem``.

        Example: ``./a/b/c.txt`` is a ``FileItem`` (i.e. a child of ``PathItem``), then this table matches it with ``./a/b`` ``DirectoryItem``

        In a bigger picture, the role of ``_internal('directories')`` item is as follows:

        1) Create ``_internal('directories')`` with ``wd='.'``, and include ``'.'`` as its only element (for now).
        
        2) Recursion on storage initialization: when a new ``PathItem`` is registered (it is contained in some directory within ``'.'``), we create an underlying``DirectoryItem`` and record the correspondence ``the PathItem => the DirectoryItem`` in ``_internal('directories')``. Repeat step 2 for the ``DirectoryItem`` until we hit ``'.'`` (no more underlying levels of directories to be considered).

        3) Recursion when a new path is registered (work phase): before creating a new element of any ``PathItem``, we first refer to ``_internal('directories')`` to verify that underlying directory exists for that new element. If no, then we first need to register and create that directory - repeat step 3 for the underlying ``DirectoryItem``.
        """
        self._new_item(
            name=self._internal('directories'),
            item_type='dirs',
            mask='.',
            wd='.',
            skip_autoindexing=True,
            additional_keys={
                self._internal('itemname'): ColumnType.UNIQUE,
                self._internal('dirobject'): ColumnType.CONSTANT,
            }
        )
        # print(repr(self.data[self._internal('directories')]['table']))

    def _initialize_modtimes_item(self) -> None:
        """Initialize ``_internal('modtimes')`` table that tracks modifiction times of all elements of all other items.
        ``_internal('modtimes')`` is an ``ObjectItem`` because it stores values of float type (this is the variable ``modtime`` column).
        Columns that uniquely identify each element of each ``DataItem`` are (1) the name of the item, (2) ID of the restriction, (3) index of the element in the corresponding table.
        """
        self._new_item(
            name=self._internal('modtimes'),
            item_type='object',
            keys=[],
            additional_keys={
                self._internal('itemname'): ColumnType.UNIQUE,
                self._internal('restriction_id'): ColumnType.UNIQUE,
                self._internal('element_index'): ColumnType.UNIQUE,
                self._internal('modtime'): ColumnType.VARIABLE,
            }
        )

    def new_restricted_item(self,
            name: str,
            key_restrictions: dict,
            restriction_id: int=None,
            constructor_kwargs={}
        ):
        """Initialize a new restriction of an existing ``DataItem``.
        
        The main purpose of restrictions is to 'hide' some (or all) keys of the main ``DataItem`` by constraining them at certain values. Thus, any function that processes the restriction does not have to know or care whether the 'hidden' keys exist. When that function is done, restricted ``DataItem`` can be merged into the main one via ``__iadd__`` (i.e. ``+=`` operator overload). Creating restrictions from another restrictions is currently not allowed (but why one would need this?).

        TODO Life-cycle of a read-only restriction:

        TODO Life-cycle of a write-only restriction:

        Args:
            name (str): name of the base ``DataItem``
            key_restrictions (dict): key restrictions to be imposed on the new ``DataItem``
            restriction_id (int, optional): restriction ID in cases when a new restriction is created not from the main ``DataItem`` but from another restriction. Defaults to None.
            constructor_kwargs (dict, optional): will be passed to constructor of the new ``DataItem``. Defaults to ``{}``.

        Returns:
            DataItem or a child: the created restriction (note that `checkin` method is not called).
        """

        assert restriction_id is None, 'Not implemented' # TODO Implement construction of restrictions of other restrictions
        self._ensure_nonlazy_item(name)

        # Get ID for new restriction
        new_restriction_id = 0
        for id in sorted(self.data[name]['restrictions'].keys()):
            if id == new_restriction_id:
                new_restriction_id += 1
            else:
                break
        
        item_type = self.data[name]['type']
        data_item = REGISTERED_DATAITEM_TYPES[item_type](
            name=name,
            storage=self,
            restrictions=copy.copy(key_restrictions),
            restriction_id=new_restriction_id,
            **constructor_kwargs,
        )

        self.data[name]['restrictions'][new_restriction_id] = {
            'name': name,
            'fixed_keys': data_item.restrictions,
            'item': data_item,

            # Keep the original rows that comply with restrictions. TODO Add kwarg to remove fixed columns
            'table': self._request_table(
                name=name,
                key_restrictions=key_restrictions,
                restriction_id=restriction_id,
            ).copy(),
        }
        return data_item

    def _create_table(self, keys: List[str]) -> pd.DataFrame:
        """Prepare empty pandas DataFrame that will later be used to
        access/filter/include separate elements by values of their keys.

        Args:
            keys (List[str]): key list of the DataItem being created.

        Returns:
            pandas.DataFrame: Empty table for the ``DataItem`` that is being registered.
        """
        return pd.DataFrame(columns=keys)

    def _ensure_nonlazy_item(self, name: str) -> None:
        data_item = self.data[name]['item']
        if data_item.lazy_mode:
            self.data[name]['table'] = self._create_table(data_item._key_names)
            data_item.lazy_mode = False
            data_item.checkin()

    def _access_item_record(self,
            name: str,
            key_restrictions: dict,
            restriction_id: int = None,
            no_additional_restrictions: bool = False
        ) -> dict:
        """This method abstracts access to the main record of ``DataItem``, as well as,
        access to records of its restrictions. NOTE: If there are some extra restrictions in ``key_restrictions``, they are not applied in this method - it only can check for absence of extra restrictions when ``no_additional_restrictions == True``

        Args:
            name (str): name of the ``DataItem`` of interest
            key_restrictions (dict): key restrictions to be applied. When accessing elements of restriction, have to pass the entire set of restrictions (as you are accessing elements from base ``DataItem``)
            restriction_id (int, optional): ID of restricted item
            no_additional_restrictions (bool): enables additional assertion that no restrictions are present (except for the base restrictions of restricted DataItems), i.e. check that ``key_restrictions`` is empty or redundant.

        Returns:
            dict: record in ``self.data``. Either main record ``self.data[name]`` or record of a restriction ``self.data[name]['restrictions'][restriction_id]``
        """

        assert name in self.data, f"DataItem with name '{name}' is not registered"
        self._ensure_nonlazy_item(name=name)

        if restriction_id is None: # Base Dataitem (not a restriction)
            if no_additional_restrictions:
                # In the context of base Dataitem, no_additional_restrictions means no restrictions at all
                assert len(key_restrictions) == 0, \
                    f"Unexpected restrictions are provided: {repr(key_restrictions)}"
                
            item_record: dict = self.data[name]
            
        else: # This is a restriction (not a base DataItem)
            assert restriction_id in self.data[name]['restrictions'], \
                f"Restriction with ID={restriction_id} of DataItem '{name}' is not registered"

            item_record: dict = self.data[name]['restrictions'][restriction_id]
            
            assert all(
                    key_value_pair in key_restrictions.items()
                    for key_value_pair in item_record['fixed_keys'].items()
                ), \
                "Restrictions provided do not conform to base restrictions imposed on restricted DataItem " \
                f"({repr(item_record['fixed_keys'])} not in {repr(key_restrictions)})"
            
            if no_additional_restrictions:
                # Check that provided restrictions exactly match the internal fixed keys of the DataItem restriction
                assert key_restrictions == item_record['fixed_keys'], \
                    "Unexpected restrictions are provided " \
                    f"({repr(item_record['fixed_keys'])} != {repr(key_restrictions)})"
            
        return item_record

    @staticmethod
    def _get_available_index(df: pd.DataFrame) -> int:
        """Get the smallest available index in the given table (``DataFrame``). Since storage table might be continuously adding/removing elements, it's nice to use the smallest possible indices (they can be arbirtary).

        Args:
            df (pandas.DataFrame): the table of interest

        Returns:
            int: the smallest available index
        """
        index = 0
        while index in df.index:
            index += 1
        return index

    def mark_lazily_available(self,
        name: str,
        key_restrictions: dict,
        restriction_id: int = None,
        no_additional_restrictions: bool = False
    ) -> None:
        assert len(key_restrictions) == 0
        assert restriction_id is None
        assert not no_additional_restrictions

        data_item = self.data[name]['item']
        assert data_item.lazy_mode
        self.data[name]['lazily_available'] = True

    
    def check_lazily_available(self,
        name: str,
        key_restrictions: dict,
        restriction_id: int = None,
        no_additional_restrictions: bool = False
    ) -> None:
        assert len(key_restrictions) == 0
        assert restriction_id is None
        assert not no_additional_restrictions
        assert self.data[name]['item'].lazy_mode

        return self.data[name]['lazily_available']

    def index_element(self, keys: dict, index=None, **request) -> None:
        """Add/update an element to indexing table of the requested ``DataItem``.

        The following steps are taken in ``index_element``:

        1) Access the DataItem to see whether such element already registered.

        2) If such element exists and ``allow_overwrite`` is enabled, then check constant keys (that new constant keys match the old ones) and remove this element from the table.

        3) Include new element into the table.

        4) Record modification time as either the current time (in case of ObjectItems), or modification time (in case of *existing* PathItems). Disable this step when registering to the ``_internal('modtimes')`` item.

        Args:
            keys (dict): values of keys for the new element
            index (Any): requested index of the new element in the ``DataItem``
            **request: ``name`` - the name of ``DataItem``, ``restriction_id`` (optional) - ID of the ``DataItem`` restriction if needed, ``key_restrictions`` (optional) - values of restricted keys if item is restricted
        """

        item_record = self._access_item_record(**request)
        item = item_record['item']
        
        # Here we construct a request for a small table will contains either:
        # 1 element, if the element with given keys is already registered
        # empty, if no elements with given keys had been registered yet
        
        # 'check_request' is a blueprint of the future request
        check_request = {
            key: value
            for key, value in request.items()
            if key != 'key_restrictions' # 'key_restrictions' are inserted separately
        }

        # Here we include 'key_restrictions' in the request
        raw_restrictions = {**keys, **request['key_restrictions']}
        final_restrictions = {
            key: value
            for key, value in raw_restrictions.items()
            # Skipping constant additional keys (key in item.additional_keys and item.additional_keys[key] == ColumnType.CONSTANT)
            # TODO What kind of keys does this 'if' trying to avoid?
            if  key in item.public_keys or \
                key in item.restrictions or \
                (key in item.additional_keys and item.additional_keys[key] == ColumnType.UNIQUE)
        }
        check_request['key_restrictions'] = final_restrictions

        # This request yields a table of 1 or 0 elements
        # no pair elements is allowed to have the same sets of UNIQUE keys
        short_df = self._request_table(**check_request)
        assert len(short_df) <= 1, \
            f"Multiple items have keys {repr(final_restrictions)}"
        
        # If 'drop_item' is True, then we will have to overwrite an already existing element
        drop_item = len(short_df) == 1

        if drop_item: # Drop old item to overwrite with new one
            assert item_record['item'].allow_overwrite, \
                f"DataStorage already contains the element of '{item_record['item'].name}' with keys {repr(final_restrictions)}, but overwrite is not allowed"
            old_index = short_df.index[0]

            # Assertions for constant keys
            old_keys = short_df.loc[old_index].to_dict()
            old_const_keys = {
                key: value
                for key, value in old_keys.items()
                if  key in item.additional_keys and \
                    item.additional_keys[key] == ColumnType.CONSTANT
            }

            for key, old_value in old_const_keys.items():
                # Need to use 'raw_restrictions' because constant keys are removed in 'final_restrictions'
                assert raw_restrictions[key] == old_value, \
                    f"Provided value of constant key '{key}' = '{raw_restrictions[key]}' does not match the previous value that is going to be overwritten = '{old_value}"

            # If we expect index to be CONSTANT, assert that it really is
            if index is not None and item.index_expectation == ColumnType.CONSTANT:
                # If index is CONSTANT, then index of the new element must be provided by the client
                assert index == old_index, \
                    f"Provided index '{index}' does not match previous index of the element that is going to be overwritten = '{old_index} (this would not be a problem, if index expectation would be VARIABLE, but it is CONSTANT)"
            # But for now, only VARIABLE indices are used

            # Remove the row that has to be overwritten
            item_record['table'].drop(old_index, inplace=True)
        
        if index is None:
            assert item.index_expectation != ColumnType.CONSTANT, \
                f"Index expectation is CONSTANT (i.e. it has to be meaningful), so it is not allowed to be assigned with random value. 'index' must be explicitly provided when registering a new element"
            index = self._get_available_index(item_record['table'])
        
        # Finally, add key-value pairs of the new element to the table of DataItem (or its restriction)
        item_record['table'].loc[index] = keys
        self.log.debug(f"Item {item_record['name']} indexed element {index} - {repr(keys)}")

        # DataStorage has a dedicated table (hidden ObjectItem) that tracks modification times of elements of all other items.
        # Since a new element is registered, _internal('modtimes') has to add/update modification time of the element.

        # This `if` avoids infinite recursion.
        # We don't want to track modification times of modificaton times themselves :)
        # TODO An even better way to avoid recursion is to disable modtime_control in the ``_internal('modtimes')`` item itself
        if request['name'] != self._internal('modtimes'):
            modtime_control = item_record['item'].modtime_control
            assert modtime_control is not None, \
                f"The item '{item_record['item'].name}' does not specify how to control modification times of its elements"

            do_not_record = False
            if modtime_control == 'manual':
                modtime = time.time()
            elif isinstance(modtime_control, dict) and \
                        len(modtime_control) == 1 and \
                        'column' in modtime_control:
                pathname = keys[modtime_control['column']]
                if os.path.isdir(pathname) or os.path.isfile(pathname):
                    modtime = os.path.getmtime(pathname)
                else:
                    # Take this branch when file/directory does not exist
                    # TODO: Why would we even register a non-existent path?
                    do_not_record = True
            else:
                raise Exception(f"Unexpected format of modtime_control of item '{item_record['item'].name}': {modtime_control}")
            
            if not do_not_record:
                self.index_element(
                    {
                        self._internal('itemname'): request['name'],
                        self._internal('restriction_id'): request['restriction_id'] 
                            if request['restriction_id'] is not None else np.nan,
                        self._internal('element_index'): index,
                        self._internal('modtime'): modtime,
                    },
                    **self._get_modtimes_request_header()
                )

    def _request_table(self, index_restrictions: list=None, **request) -> pd.DataFrame:
        """Get table of elements for a given request

        Args:
            **request: ``name`` - the name of DataItem, ``restriction_id`` (optional) - ID of the DataItem restriction if needed, ``key_restrictions`` (optional) - values of restricted keys if item is restricted

        Returns:
            pandas.DataFrame: the requested table
        """

        # Access the required record in self.data and extract the table
        item_record: dict = self._access_item_record(**request)
        df: pd.DataFrame = item_record['table']

        if index_restrictions is not None:
            raise Warning(f"Non _request_table functionality with non-trivial 'index_restrictions' was not tested yet. index_restrictions={repr(index_restrictions)}")
            df = df[df.index.isin(index_restrictions)]
        
        # If there are any extra restrictions in request['key_restrictions'], '_access_item_record' does not process them
        # So here we probably have to do some filtering
        result: pd.DataFrame = self._extract_subdataframe(df, request['key_restrictions'])
        return result

    def _assign_table(self, new_df: pd.DataFrame, **request) -> None:
        """Overwrite a table in the record of some DataItem with a new one (``new_df``).

        Args:
            new_df (pandas.DataFrame): a table that will overwrite the old one
            **request: ``name`` - the name of DataItem, ``restriction_id`` (optional) - ID of the DataItem restriction if needed, ``key_restrictions`` (optional) - values of restricted keys if item is restricted.
        """

        # 'no_additional_restrictions' enables further assertion to fully verify
        # that the request for specific table does not contain anything unreasonable (e.g. extra key restrictions)
        item_record: dict = self._access_item_record(**request, no_additional_restrictions=True)
        
        # Not doing any extensive checks of `new_df` since this method is not supposed
        # to be called from outside of DataStorage
        item_record['table'] = new_df

    def unbind_restriction(self,
            name: str,
            key_restrictions: dict={},
            restriction_id: int=None
        ) -> None:
        """Unregister a restriction that has index ``restriction_id`` from DataItem ``name``. As a precaution against client mistakes, ``key_restrictions`` must precisely match the fixed keys of the item restriction.

        Restriction is deleted in several steps:

        1. Verify that ``key_restrictions`` and ``restriction_id`` are non-trivial and the restriction is unambiguously specified (user-provided ``key_restrictions`` are correct)

        2. Remove the restriction from ``self.data``

        3. Remove all timing information that was stored for that restriction. TODO: Does it transfer timings when adding restriction to the main DataItem?

        Args:
            name (str): name of the DataItem of interest
            key_restrictions (dict, optional): values of restricted keys if item is restricted. Defaults to {}.
            restriction_id (int, optional): ID of the DataItem restriction if needed. Defaults to None.

        Raises:
            RuntimeError: If unbinding of a non-restricted DataItem is requested
        """

        if restriction_id is None:
            raise RuntimeError('Cannot unbind DataItem that is not a restriction of another DataItem')
        
        # This is just to go over a few assertions impelemented in _access_item_record
        self._access_item_record(
            name=name,
            key_restrictions=key_restrictions,
            restriction_id=restriction_id,
            no_additional_restrictions=True
        )

        # Actual removal - unlinking from ``self.data[name]['restrictions']``
        del self.data[name]['restrictions'][restriction_id]

        # Remove *all* elements in ``_internal('modtimes')`` that corresponded to that restriction
        self.remove_elements(
            removed_elements_specs=[
                {
                    'keys': {
                        # This dict describes a single set of search constraints that select all elements to be removed from modtime table
                        self._internal('itemname'): name,
                        self._internal('restriction_id'): restriction_id,
                    }
                }
            ],
            allow_multiple_rows=True,
            **self._get_modtimes_request_header()
        )

    def process_request(self,
            return_value: str = 'keys',
            expect_single_element: bool = False,
            **request
        ) -> int | str | Tuple | Dict | List:
        """More user-friendly API to be used instead of raw ``self._request_table``. Three options are available: obtain either indices or keys of the requested elements, or both:
        
        1) ``return_value == 'keys'``: return only key(s) of the requested element(s)

        2) ``return_value == 'index'``: return only index(indices) of the requested element(s)

        3) ``return_value == 'keys&index'``: return both keys+indices of the requested element(s)

        When client can guarantee that the request matches exactly one element, ``expect_single_element`` can be set ``True`` - this enabled the uniqueness checks and return format is more simple since there is no need to enclose a single element in a container.
        
        Overall, there are 6 ways to format the requested data:

        * ``expect_single_element``

            1. ``return_value=='keys'``: Dict[key, value]
                
            2. ``return_value=='index'``: value of the index
            
            3. ``return_value=='keys&index'``: Tuple[value of the index, Dict[key, value]]

        * ``NOT expect_single_element``

            1. ``return_value=='keys'``: List[Dict[key, value]]
                
            2. ``return_value=='index'``: List[index values]
            
            3. ``return_value=='keys&index'``: Dict[index value, Dict[key, value]]
        
        Args:
            return_value (str, optional): Values to be returned for requested elements: 'keys', 'index', or 'keys&index'. Defaults to 'keys'.
            expect_single_element (bool, optional): Enables additional assertions and simpler result formatting when only one element is expected to be returned. Defaults to False.

        Returns:
            int | str | typing.Tuple | typing.Dict | typing.List: The key(s), index(indices) or both for all requested elements
        """
        
        df: pd.DataFrame = self._request_table(**request)

        assert return_value in ('keys', 'index', 'keys&index'), \
            f"Only three 'return_value' options are available: ('keys', 'index', 'keys&index'). Got: {return_value}"

        if expect_single_element:
            assert len(df) > 0, f"Element corresponding to request '{repr(request)}' not found"
            assert len(df) == 1, f"Multiple elements corresponding to request '{repr(request)}' were found, but 'expect_single_element' is enabled"
        
            if return_value == 'keys':
                df_dict = df.to_dict(orient='index')
                index, keys = next(iter(df_dict.items()))
                return keys
            elif return_value == 'index':
                return df.index[0]
            else: # 'keys&index'
                df_dict = df.to_dict()
                return df_dict
        else:
            if return_value == 'keys':
                df_dict = df.to_dict(orient='records')
                return df_dict
            elif return_value == 'index':
                return df.index.to_list()
            else: # 'keys&index'
                df_dict = df.to_dict()
                return df_dict

    def rename_indices(self, index_mapping: dict, **request):
        """Rename indices in the table specified by ``**request``.

        Args:
            index_mapping (dict): index mapping for renaming: keys => values (keys=old_index, value=new_index)
            **request: ``name`` - the name of DataItem, ``restriction_id`` (optional) - ID of the DataItem restriction if needed, ``key_restrictions`` (optional) - values of restricted keys if item is restricted.
        """

        df: pd.DataFrame = self._request_table(**request)
        df = df.rename(index=index_mapping)
        self._assign_table(df, **request)

    def access_key_table(self, **request) -> pd.DataFrame:
        """Access the table that matches indices of elements with values of all keys in the requested ``DataItem``.

        Args:
            **request: ``name`` - the name of DataItem, ``restriction_id`` (optional) - ID of the DataItem restriction if needed, ``key_restrictions`` (optional) - values of restricted keys if item is restricted.
        
        Returns:
            pandas.DataFrame: dataframe (NOT a copy!) that stores indices, keys and they values
        """

        return self._access_item_record(
            **request,
            no_additional_restrictions=True
        )['table']

    def unique_keyvalues(self, keyname: str, /, **request) -> list:
        """Get unique values of key ``keyname`` in the requested ``DataItem``

        Args:
            keyname (str): name of the key
            **request: ``name`` - the name of DataItem, ``restriction_id`` (optional) - ID of the DataItem restriction if needed, ``key_restrictions`` (optional) - values of restricted keys if item is restricted.

        Returns:
            list: list of unique values of the key ``keyname``
        """

        df: pd.DataFrame = self._request_table(**request)
        return df[keyname].unique().tolist()

    def iter(self, **request) -> Iterable[Dict[Literal['index', 'keys'], int | str | dict]]:
        """Iterate over rows of a table correspoing to the requested ``DataItem``

        Args:
            **request: ``name`` - the name of DataItem, ``restriction_id`` (optional) - ID of the DataItem restriction if needed, ``key_restrictions`` (optional) - values of restricted keys if item is restricted.

        Returns:
            typing.Iterable[typing.Dict[typing.Literal['index', 'keys'], int | str | dict]]: returning each element as ``{'index': index, 'keys': dict of key-value pairs}``
        """
        return ItemTableIterator(self, **request)

    def __iter__(self) -> Iterable[Tuple[str, Any]]:
        """Iterate over all DataItems in the storage. Skips hidden DataItem (i.e. containers of project directories and modification times)

        TODO: Remember how it works

        Returns:
            Iterable[Tuple[str, DataItem or child]]: 
        """
        return StoredItemsIterator(self)

    def check_item(self, **request) -> bool:
        """Check if requested element exists. Only complete sets of key values must be provided.

        Args:
            **request: ``name`` - the name of DataItem, ``restriction_id`` (optional) - ID of the DataItem restriction if needed, ``key_restrictions`` (optional) - values of restricted keys if item is restricted.

        Returns:
            bool: whether any element has been found
        
        Raises:
            AssertionError: incomplete set of keys was provided
        """
        df = self._request_table(**request)
        assert len(df) == 1 or len(df) == 0, \
            f"Unexpected length of df = {len(df)}"
        return len(df) == 1
    
    def contains_index(self, index: int | str, **request) -> bool:
        """Check if a row with given index is present in the table of the requested ``DataItem``

        Args:
            index (int | str): index of interest
            **request: ``name`` - the name of DataItem, ``restriction_id`` (optional) - ID of the DataItem restriction if needed, ``key_restrictions`` (optional) - values of restricted keys if item is restricted.

        Returns:
            bool: if row with a given index was found
        """

        df: pd.DataFrame = self._request_table(**request)
        return index in df.index
    
    def _get_dirs_request(self) -> dict:
        """Generate a header for request ``_internal('directories')``. See ``self._initialize_directories_item`` for further info.

        Returns:
            dict: This dict can be passed as ``**request`` to all methods of the storage in order to work with internal table of project directories.
        """
        return {
            'name': self._internal('directories'),
            'key_restrictions': {},
            'restriction_id': None,
        }
    
    def _get_modtimes_request_header(self) -> dict:
        """Generate a header for request ``_internal('modtimes')``.

        Returns:
            dict: This dict can be passed as ``**request`` to all methods of the storage in order to work with internal table of project modification times.
        """
        return {
            'name': self._internal('modtimes'),
            'key_restrictions': {},
            'restriction_id': None,
        }

    def _get_containing_diritem(self,
            path: str=None,
            itemname: str=None
        ) -> str | None:
        """Determine the name of DirectoryItem what contains the provided ``path`` or PathItem ``itemname`` (one of these has to be provided).

        Args:
            path (str, optional): The path for which a containing directory has to be determined. Defaults to None.
            itemname (str, optional): The name of any registered PathItem for which a containing directory has to be determined. Defaults to None.

        Returns:
            str | None: ``str`` - the name of containing DirectoryItem if is was found. ``None`` - otherwise.
        """

        assert (path is not None) ^ (itemname is not None), \
            f"Only one of 'path'/'itemname' must be provided. Got path={path}, itemname={itemname}"

        # Construct request to `_internal('directories')` that stores both itemnames and
        # corresponding paths to containing dirs
        dirs_request: Dict[str, Any] = self._get_dirs_request()
        if path is not None:
            dirs_request['key_restrictions'] = {
                self._internal('path'): path,
            }
        else:
            dirs_request['key_restrictions'] = {
                self._internal('itemname'): itemname,
            }

        found_items: List[Dict[str, Any]] = self.process_request(return_value='keys', **dirs_request)
        # Each dict contains the key `self._internal('dirobject')` - the name of DirectoryItem that we are looking for

        if len(found_items) == 0:
            return None
        
        assert len(
                set((
                    keys[self._internal('dirobject')]
                    for keys in found_items
                ))
            ) == 1, \
            f"Multiple DirectoryItems describe the same path/itemname"
        
        dir_object: str = found_items[0][self._internal('dirobject')]
        return dir_object

    def request_base_directory(self,
            dirname: str,
            **request
        ) -> None:
        """Index ``dirname`` as a new working directory mask that corresponds to ``PathItem`` specified by ``**request``. It will create appropriate DirectoryItems, if they do not yet exist, and register the ``PathItem <=> containing DirectoryItem`` relationship in the ``self._internal('directories')`` table.

        This method is called by each ``PathItem`` during ``checkin()`` (the 2nd stage of initialization).

        Args:
            dirname (str): The directory mask to be indexed.
            **request: ``name`` - the name of DataItem, ``restriction_id`` (optional) - ID of the DataItem restriction if needed, ``key_restrictions`` (optional) - values of restricted keys if item is restricted.
        """

        # Check if 'dirname' is located in the root of the storage (e.g. for FileItem "./{x}.{ext}", dirname is '.')
        diritem_is_trivial: bool = (dirname == '.')

        if diritem_is_trivial:
            # Per agreement, the containing DirectoryItem is `self._internal('directories')` itself (its mask is '.')
            diritem_name: str = self._internal('directories')
        else:
            # Check if appropriate DirectoryItem is already registered (str - registered, None - not)
            diritem_name: str | None = self._get_containing_diritem(path=dirname)

            if diritem_name is None:
                # If no DirectoryItems describing the requested dirname were found, create a new one
                new_diritem_name: str = self._internal('dirtemplate').format(itemname=request['name'])
                self._new_item(
                    name=new_diritem_name,
                    item_type='dirs',
                    mask=dirname,
                    **self.additional_kwargs
                )
                diritem_name: str = new_diritem_name
        
        # In any case, the relationship between PathItem described by **request and
        # DirectoryItem ``diritem_name`` must be registered in the dedicated table 
        self.index_element(
            {
                self._internal('path'): dirname,
                self._internal('itemname'): request['name'],
                self._internal('dirobject'): diritem_name,
            },
            **self._get_dirs_request()
        )
    
    def number_of_elements(self, **request) -> int:
        """Get the number of elements contained in the requested ``DataItem``.

        Args:
            **request: ``name`` - the name of DataItem, ``restriction_id`` (optional) - ID of the DataItem restriction if needed, ``key_restrictions`` (optional) - values of restricted keys if item is restricted.

        Returns:
            int: the number of elements (rows in the table)
        """
        df: pd.DataFrame = self._request_table(**request)
        return df.shape[0]

    def __contains__(self, item_name: str) -> bool:
        """Check whether the storage contains DataItem with the name ``item_name``.

        Args:
            item_name (str): name of the ``DataItem`` client is looking for

        Returns:
            bool: ``True`` - item with this name is found, ``False`` - otherwise
        """
        assert item_name not in DataStorage.INTERNALS['items'], \
            f"The item name '{item_name}' is protected"
        return item_name in self.data

    def __getattr__(self, item_name: str):
        """Enables the dot-access to DataItems, i.e. ``storage.item_name``

        Args:
            item_name (str): name of the ``DataItem`` of interest

        Returns:
            DataItem or child: the required ``DataItem`` (or a child class) object

        Raises:
            AssertionError: Name of nonregistered ``DataItem`` or some unknown attrubute is provided
        """

        assert item_name in self.data, f"Item with name '{item_name}' is not registered"
        return self.data[item_name]['item']
    
    def get_item(self, item_name: str):
        assert item_name in self.data, f"Item with name '{item_name}' is not registered"
        return self.data[item_name]['item']

    def propose_storage_directory(self, item_name: str) -> str:
        """If the user didn't request any specific directory layout for storing some ``PathItem``, the ``PathItem`` class automatically decides to store all elements in a single directory. The name of that directory is generated by this method.

        Args:
            item_name (str): The name of ``PathItem`` makes this request

        Returns:
            str: The directory of the storage where all elements of ``item_name`` will be stored.
        """
        abspath: str = os.path.join(self.wd, f"{item_name}_data")
        relpath: str = self.preprocess_wd(abspath)
        return relpath

    def preprocess_wd(self, provided_wd: str) -> str:
        """Transforms the containing directory mask of a ``PathItem`` to the standard format, i.e. relative path to the ``storage.wd``.

        Args:
            provided_wd (str): Path to containing directory that has to be standardized.

        Returns:
            str: The standardized path to directory ``provided_wd``, i.e. the path relative to ``storage.wd``. The format is './a/b/c' (if nontrivial, should start with './') or just '.' (if trivial).
        """
        if provided_wd == '.':
            return '.'
        
        provided_wd: str = os.path.normpath(provided_wd)
        if os.path.isabs(provided_wd): # Absolute path
            assert Path(provided_wd).is_relative_to(Path(self.wd)), \
                f"PathItems must be stored inside '{self.wd}' which is the storage for project files"
        elif len(os.path.split(provided_wd)) > 1: # Relative path
            provided_wd: str = os.path.abspath(os.path.join(self.wd, provided_wd))
            
        relpath: str = os.path.relpath(provided_wd, self.wd)
        # ``relpath`` is either '.' or 'a/b/c'. If it's not '.', we want it to start with './'
        if relpath != '.':
            # Doing this to '.' leads to './.' and this would break all the logic
            relpath: str = os.path.join('.', relpath)
        return relpath

    def preprocess_mask(self, mask: str) -> Tuple[str, str | None]:
        """Transform a path mask of some file/directory in the storage into a pair: base name + containing directory in standardized format.
        
        For example, ``'/absolute/path/to/storage/{keyA}_files/{keyB}_file.ext'`` transforms into ``('{keyB}_file.ext', './{keyA}_files')`` and the result is completely the same for relative input path mask: ``'./{keyA}_files/{keyB}_file.ext'``. If ``mask='{keyB}_file.ext'``, the resulting tuple will be ``('{keyB}_file.ext', None)``

        Args:
            mask (str): The path mask to elements of some ``PathItem``.

        Returns:
            Tuple[str, str | None]: First element - base name of the file/directory, second element - ``None`` if no containing directory was specified in the input ``mask``. Otherwise, standardized path mask to containing directory (see ``self.preprocess_wd`` for the rules of standardization).
        """
        if len(os.path.split(mask)[0]) > 0:
            mask_path = Path(mask)
            return mask_path.name, self.preprocess_wd(str(mask_path.parent))
        else:
            return mask, None

    def postprocess_path(self, provided_relpath: str, absolute: bool=False) -> str:
        if not absolute:
            assert not os.path.isabs(provided_relpath)
            return os.path.normpath(os.path.join(self.wd, provided_relpath)) # TODO THIS **IS** AN ABSOLUTE PATH!!!
        else:
            assert Path(provided_relpath).is_relative_to(Path(self.wd)), \
                f"Provided {provided_relpath} is not relative to {self.wd}"
            return provided_relpath  # TODO AND THIS IS TOO!!!

    def verify_parent_directory(self, name: str, **kwargs):
        diritem = self.get_containing_diritem(name=name)
        self.verify_directory(diritem=diritem, **kwargs)

    def verify_directory(self, diritem, key_restrictions: dict, **kwargs):
        diritem_name = diritem.name

        if diritem_name == self._internal('directories'):
            dir_keyvalues = {}
        else:
            dir_keyvalues = {
                key: value
                for key, value in key_restrictions.items()
                if key in diritem.public_keys
            }
        dirname = diritem.get_path(**dir_keyvalues)

        if not os.path.isdir(dirname):
            self.log.info(f"Creating directory '{dirname}'")
            os.mkdir(dirname)

            if diritem_name != self._internal('directories'):
                diritem.include_element(
                    filename=dirname,
                    **dir_keyvalues
                )
        else:
            self.log.debug(f"Directory '{dirname}' already exists")
    
    def get_containing_diritem(self, name: str, key_restrictions: dict={}, **kwargs):
        diritem_name = self._get_containing_diritem(itemname=name)
        assert diritem_name is not None, f"Cannot find DirItem containing the item '{name}'"
        
        res_item = self.data[diritem_name]['item']
        diritem_restriction = {
            key: value
            for key, value in key_restrictions.items()
            if key in res_item.public_keys
        }
        if len(diritem_restriction) == 0:
            return res_item
        else:
            return res_item.get_restricted(keys=diritem_restriction)

    @staticmethod
    def entry_extends(entry, test_entry):
        return all(
            key in test_entry and test_entry[key] == value
            for key, value in entry.items()
        )

    def clear_table(self, **request) -> None:
        df = self._request_table(**request)
        
        # Remove all rows from the table to clean it
        df.drop(df.index, inplace=True)

    def deallocate_directories(self, 
            name: str,
            key_restrictions: dict,
            restriction_id: int=None,
            **kwargs
        ) -> None:
        
        assert restriction_id is None, f"Cannot deallocate directories of restriced item '{name}'"
        
        diritem = self.get_containing_diritem(name=name)
        diritem_name = diritem.name
        
        if diritem_name != self._internal('directories'):
            diritem.cleanup()
    
    def remove_elements(self,
            removed_elements_specs: List[Dict[str, Any]],
            allow_multiple_rows: bool=False,
            **request
        ) -> None:
        """_summary_

        Args:
            removed_elements_specs (List[Dict[str, Any]]): _description_
            allow_multiple_rows (bool, optional): _description_. Defaults to False.
            **request: ``name`` - the name of DataItem, ``restriction_id`` (optional) - ID of the DataItem restriction if needed, ``key_restrictions`` (optional) - values of restricted keys if item is restricted.

        Raises:
            RuntimeError: _description_
        """
        
        # Going to delete some rows of this table
        df: pd.DataFrame = self._request_table(**request)

        remove_indices = [] # Will collect indices of the rows to remove
        for current_element_specs in removed_elements_specs:
            # On each iteration, will identify one or several element(s) to be removed
            
            current_element_specs: Dict[Literal['index', 'keys'], int | str | dict]
            # Use different logic depending on the provided information ('index', 'keys', or both)

            if 'index' in current_element_specs and 'keys' in current_element_specs:
                # In this case, user provided both index and key-value pairs, so
                # we can verify the consistency of key-value pairs with index before deleting

                # In this mode, only element is removed per iteration (per ``current_element_specs``)

                provided_index: int | str | Any = current_element_specs['index']
                provided_keys: Dict[str, Any] = current_element_specs['keys']
                actual_keys: Dict[str, Any] = df.loc[provided_index].to_dict()
                assert actual_keys == provided_keys, \
                    f"Mismatch between provided and actual keys for the element with index {provided_index}: provided={repr(provided_keys)} != actual={repr(actual_keys)}"
                remove_indices.append(provided_index)

            elif 'keys' in current_element_specs:
                provided_keys: Dict[str, Any] = current_element_specs['keys']
                sub_df: pd.DataFrame = self._extract_subdataframe(df, provided_keys)
                
                # In this mode, removal of multiple elements is possible,
                # so ``allow_multiple_rows`` safeguards against user mistakes
                if not allow_multiple_rows:
                    assert len(sub_df) == 1, \
                        f"Removal of multiple elements is disabled, but provided keys {repr(provided_keys)} correspond to multiple elements of {request['name']}:\n{sub_df}"
                    index = sub_df.index[0]
                    remove_indices.append(index)
                else:
                    remove_indices += sub_df.index.to_list()

            elif 'index' in current_element_specs:
                # In the index-only removal, no verifications can be done
                # Only element is removed per iteration (per ``current_element_specs``)
                remove_indices.append(index)

            else:
                raise RuntimeError(f"Cannot process this request for element(s) removal: {repr(current_element_specs)}")
        
        # Remove elements based on collected indices
        df.drop(remove_indices, inplace=True)

    @staticmethod
    def _extract_subdataframe(
            df: pd.DataFrame,
            restriction_dict: Dict[str, Any]
        ) -> pd.DataFrame:
        """Constructs a resticted DataFrame, when given a full DataFrame and several restrictions as a dict.

        Args:
            df (pandas.DataFrame): the starting DataFrame (it won't be changed when restriction is created)
            restriction_dict (Dict[str, Any]): dict that maps key to their required values

        Returns:
            pandas.DataFrame: the resulting restricted DataFrame. It contains those rows of the original ``df``, that comply with ``restriction_dict``. If ``restriction_dict`` is empty, no exception is raised and ``df`` is returned.
        """

        if len(restriction_dict) == 0:
            return df

        restrictions_series: pd.Series = [
            df[key] == value # == create some kind of boolean list
            for key, value in restriction_dict.items()
        ]
        total_restriction: pd.Series = restrictions_series[0]
        for item in restrictions_series[1:]:
            # here we apply AND on all boolean lists (one list per one key restriction)
            total_restriction = total_restriction & item
        return df[total_restriction]

    # TODO Implement file lookups

    @staticmethod
    def _internal_item(itemname: str) -> str:
        """To avoid conflicts between actual item names and
        internal item names using within DataStorage, we
        augment them with '_internalitem_' prefix.

        Args:
            itemname (str): meaningful name of internal item

        Returns:
            str: augmented name of internal item
        """
        return f'_internalitem_{itemname}'
    
    @staticmethod
    def _internal_key(keyname: str) -> str:
        """To avoid conflicts between actual key names and
        internal key names using within DataStorage, we
        augment them with '_internalkey_' prefix.

        Args:
            keyname (str): meaningful name of internal key

        Returns:
            str: augmented name of internal key
        """
        return f'_internalkey_{keyname}'

    def _internal(self, name: str) -> str:
        return self.INTERNALS_TOTAL[name]

    @staticmethod
    def _fstring_to_wildcard(fstring: str) -> str:
        subs = {
            t[1]: '*'
            for t in string.Formatter().parse(fstring)
            if t[1] is not None
        }
        return fstring.format(**subs)


DataStorage.INTERNALS = {
    'items': {
        'dirtemplate': DataStorage._internal_item('dirtemplate_{itemname}'),
        **{
            itemname: DataStorage._internal_item(itemname)
            for itemname in [
                # DirectoryItem: Index of all directories used to store FileItems and children, etc.
                'directories',
                'modtimes',
            ]
        }
    },
    'keys': {
        'path': PathItem.PATH_KEY,
        **{
            keyname: DataStorage._internal_key(keyname)
            for keyname in [
                # self-explanatory (for instance, 'directories' item stores wd's
                # of all items, thus, the need for 'itemname' key).
                # 'dirobject' is the DirectoryItem that handles self.wd of 'itemname'.
                'itemname',
                'dirobject',

                # For 'modtimes': 'itemname' again, and
                # 'element_index' - element of the item for which mod time is recorded
                # and 'modtime' itself
                'restriction_id',
                'element_index',
                'modtime',
            ]
        },
    },
}
DataStorage.INTERNALS_TOTAL = {
    key: value
    for internal_dict in DataStorage.INTERNALS.values()
    for key, value in internal_dict.items()
}


class ItemTableIterator:
    def __init__(self, base_instance, /, **request):
        df: pd.DataFrame = base_instance._request_table(**request)
        self.main_iter = df.iterrows()

    def __next__(self) -> dict:
        index, keys = next(self.main_iter)
        return {
            'index': index,
            'keys': keys.to_dict(),
        }


class StoredItemsIterator:
    """TODO: I have no idea what is happening here
    """
    def __init__(self, base_instance):
        self.main_iter = iter(base_instance.data.items())
        self.protected_items = [
            base_instance._fstring_to_wildcard(itename_mask)
            for itename_mask in base_instance.INTERNALS['items'].values()
        ]

    @staticmethod
    def contains_wildcard(value, mask_container):
        return any(
            fnmatch.fnmatch(value, current_mask)
            for current_mask in mask_container
        )

    def __next__(self) -> dict:
        item_name, keys = next(self.main_iter)
        while self.contains_wildcard(item_name, self.protected_items):
            item_name, keys = next(self.main_iter)
        return (item_name, keys['item'])
